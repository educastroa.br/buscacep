unit untBuscaCEP;

interface

uses
  System.Classes, System.SysUtils, Vcl.Dialogs, System.JSON, REST.Response.Adapter,
  Data.DB, DBClient, IdHTTP, IdSSLOpenSSL;

type
  TEstadosBR = (esSelecione, esAcre, esAlagoas, esAmap�, esAmazonas, esBahia, esCear�,
                esEsp�ritoSanto, esGoi�s, esMaranh�o, esMatoGrosso, esMatoGrossoDoSul,
                esMinasGerais, esPar�, esPara�ba, esParan�, esPernambuco, esPiau�,
                esRioDeJaneiro, esRioGrandeDoNorte, esRioGrandeDoSul, esRond�nia,
                esRoraima, esSantaCatarina, esSaoPaulo, esSergipe, esTocantins,
                esDistritoFederal);

  TBuscaCEP = Class(TObject)
  private
    FIdHTTP: TIdHTTP;
    FOpenSSL: TIdSSLIOHandlerSocketOpenSSL;
    FClientDataSet: TClientDataSet;
    procedure Criar;
    procedure Destruir;
    function Busca(PCEP: String; PLogradouro: String;
                   PMunicipio: String; PUF: String): Boolean; overload;
    procedure AbreDataSet(PJson: String);
  public
    function Busca(PCEP: String): Boolean; overload;
    function Busca(PLogradouro: String; PMunicipio: String; PUF: String): Boolean; overload;
    procedure AfterConstruction; override;
    destructor Destroy; override;
  published
    property ClientDataSet: TClientDataSet read FClientDataSet write FClientDataSet;
  end;

const
  EstadosBR : Array [TEstadosBR] of String = (
                    'Selecione', 'Acre', 'Alagoas', 'Amap�', 'Amazonas', 'Bahia',
                    'Cear�', 'Esp�rito Santo', 'Goi�s', 'Maranh�o', 'Mato Grosso',
                    'Mato Grosso do Sul', 'Minas Gerais', 'Par�', 'Para�ba', 'Paran�',
                    'Pernambuco', 'Piau�', 'Rio de Janeiro', 'Rio Grande do Norte',
                    'Rio Grande do Sul', 'Rond�nia', 'Roraima', 'Santa Catarina',
                    'S�o Paulo', 'Sergipe', 'Tocantins', 'Distrito Federal');
  EstadosBRCapitais : Array [TEstadosBR] of String = (
                    'Selecione', 'Rio Branco', 'Macei�', 'Macap�', 'Manaus', 'Salvador',
                    'Fortaleza', 'Vit�ria', 'Goi�nia', 'S�o Lu�s', 'Cuiab�', 'Campo Grande',
                    'Belo Horizonte', 'Bel�m', 'Jo�o Pessoa', 'Curitiba', 'Recife',
                    'Teresina', 'Rio de Janeiro', 'Natal', 'Porto Alegre', 'Porto Velho',
                    'Boa Vista', 'Florian�polis', 'S�o Paulo', 'Aracaju', 'Palmas', 'Bras�lia' );
  EstadosBRSigla : Array [TEstadosBR] of String = (
                           'Selecione', 'AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'ES', 'GO',
                           'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ',
                           'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO', 'DF');

implementation

function TiraAcentos(const PValor: ANSIString): ANSIString;
const
  cAcentos   = '������������������������������������������������';
  cNormais   = 'aeiouAEIOUaeiouAEIOUaeiouAEIOUaoAOaeiouAEIOUcCnN';
var
  nIndex: Integer;
begin
  Result := '';
  for nIndex := 1 to Length(PValor) do
  begin
    if Pos(PValor[nIndex], cAcentos) > 0 then
      Result := Result + cNormais[Pos(PValor[nIndex], cAcentos)]
    else
      Result := Result + PValor[nIndex];
  end;
end;

function Municipio(PMunicipio: String): String;
begin
  Result := TiraAcentos(StringReplace(PMunicipio, ' ', '%20', [rfReplaceAll]));
end;

function Endereco(PLogradouro: String): String;
begin
  Result := TiraAcentos(StringReplace(PLogradouro, ' ', '+', [rfReplaceAll]));
end;

{ TBuscaCEP }

procedure TBuscaCEP.AfterConstruction;
begin
  inherited;
  FClientDataSet := TClientDataSet.Create(Nil);
end;

function TBuscaCEP.Busca(PCEP: String): Boolean;
begin
  Result := Busca(PCEP, EmptyStr, EmptyStr, EmptyStr);
end;

function TBuscaCEP.Busca(PLogradouro, PMunicipio, PUF: String): Boolean;
begin
  Result := Busca(EmptyStr, Trim(PLogradouro), Trim(PMunicipio), Trim(PUF));
end;

function TBuscaCEP.Busca(PCEP, PLogradouro, PMunicipio, PUF: String): Boolean;
const
  URLCEP = 'https://viacep.com.br/ws/%s/json/';
  URLLog = 'https://viacep.com.br/ws/%s/%s/%s/json/';
  INVALID_CEP = '{'#$A'  "erro": true'#$A'}';

var
  sURL: String;
  sJSon: String;
  sResposta: TStringStream;

begin
  Result := True;
  sResposta := TStringStream.Create;
  try
    try
      if Trim(PCEP) <> EmptyStr then
        sURL := Format(URLCEP, [PCep.Trim])
      else
        sURL := Format(URLLog, [PUF, Municipio(PMunicipio), Endereco(PLogradouro)]);
      Criar;
      FIdHTTP.Get(sURL, sResposta);
      sJSon := EmptyStr;
      if (FIdHTTP.ResponseCode = 200) and (not (sResposta.DataString).Equals(INVALID_CEP)) then
        if Pos('[', sResposta.DataString) <= 0 then
          sJSon := '[' + sResposta.DataString + ']'
        else
          sJSon := AnsiString(sResposta.DataString)
      else
        ShowMessage('CEP ' + PCEP + ' n�o localizado!');
      if Trim(sJSon) <> EmptyStr then
        AbreDataSet(sJSon);
    except
      on E : Exception do
        ShowMessage(E.Message);
    end;
  finally
    Destruir;
    FreeAndNil(sResposta);
  end;
end;

procedure TBuscaCEP.Criar;
begin
  FIdHTTP := TIdHTTP.Create;
  FOpenSSL := TIdSSLIOHandlerSocketOpenSSL.Create;
  FIdHTTP.IOHandler := FOpenSSL;
  FOpenSSL.SSLOptions.SSLVersions := [sslvTLSv1, sslvTLSv1_1, sslvTLSv1_2];
end;

destructor TBuscaCEP.Destroy;
begin
  if Assigned(FClientDataSet) then
    FreeAndNil(FClientDataSet);
  inherited;
end;

procedure TBuscaCEP.Destruir;
begin
  if Assigned(FOpenSSL) then
    FreeAndNil(FOpenSSL);
  if Assigned(FIdHTTP) then
    FreeAndNil(FIdHTTP);
end;

procedure TBuscaCEP.AbreDataSet(PJSon: String);
var
  JObj: TJSONArray;
  vConv : TCustomJSONDataSetAdapter;
begin
  if (PJSon = EmptyStr) then
    Exit;
  FClientDataSet.Close;
  FClientDataSet.FieldDefs.Clear;
  JObj := TJSONObject.ParseJSONValue(TEncoding.Default.GetBytes(PJSon), 0) as TJSONArray;
  vConv := TCustomJSONDataSetAdapter.Create(Nil);
  try
    vConv.Dataset := FClientDataSet;
    vConv.UpdateDataSet(JObj);
  finally
    FreeAndNil(vConv);
    FreeAndNil(JObj);
  end;
end;

end.
