unit untFrmBuscaCEP;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Data.DB,
  Datasnap.DBClient, Vcl.Grids, Vcl.DBGrids, Vcl.ExtCtrls, System.Actions,
  Vcl.ActnList, Vcl.ImgList, Vcl.Buttons, untBuscaCEP, JvExDBGrids, JvDBGrid;

type
  TFrmBuscaCEP = class(TForm)
    Panel2: TPanel;
    dtsBuscaCEP: TDataSource;
    imlBuscaCEP: TImageList;
    aclBuscaCEP: TActionList;
    actBuscaCEP: TAction;
    actBuscaLogradouro: TAction;
    grdBuscaCEP: TJvDBGrid;
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    btnBuscaCEP: TSpeedButton;
    edtCEP: TEdit;
    GroupBox1: TGroupBox;
    btnBuscaLogradouro: TSpeedButton;
    Label1: TLabel;
    edtLogradouro: TEdit;
    Label2: TLabel;
    edtMunicipio: TEdit;
    Label3: TLabel;
    cbbEstado: TComboBox;
    Label4: TLabel;
    procedure actBuscaCEPExecute(Sender: TObject);
    procedure actBuscaLogradouroExecute(Sender: TObject);
    procedure edtCEPKeyPress(Sender: TObject; var Key: Char);
  private
    FCEP: String;
    FBuscaCEP: TBuscaCEP;
    procedure SetCEP(const Value: String);
  public
    procedure AfterConstruction; override;
    destructor Destroy; override;
  published
    property CEP: String read FCEP write SetCEP;
    property BuscaCEP: TBuscaCEP read FBuscaCEP write FBuscaCEP;
  end;

var
  FrmBuscaCEP: TFrmBuscaCEP;

implementation

{$R *.dfm}

procedure TFrmBuscaCEP.actBuscaCEPExecute(Sender: TObject);
begin
  if Trim(edtCEP.Text) = EmptyStr then
    Raise Exception.Create('CEP n�o informado!');
  if Length(Trim(edtCEP.Text)) < 7 then
    Raise Exception.Create('CEP incorreto!');
  edtCEP.Text := StringOfChar('0', 8 - Length(Trim(edtCEP.Text))) + Trim(edtCEP.Text);
  FBuscaCEP.Busca(edtCEP.Text);
end;

procedure TFrmBuscaCEP.actBuscaLogradouroExecute(Sender: TObject);
begin
  if Trim(edtLogradouro.Text) = EmptyStr then
  begin
    edtLogradouro.SetFocus;
    edtLogradouro.SelectAll;
    Raise Exception.Create('Logradouro de preenchimento obrigat�rio!');
  end;
  if Trim(edtMunicipio.Text) = EmptyStr then
  begin
    edtMunicipio.SetFocus;
    edtMunicipio.SelectAll;
    Raise Exception.Create('Munic�pio de preenchimento obrigat�rio!');
  end;
  if cbbEstado.ItemIndex <= 0 then
  begin
    cbbEstado.SetFocus;
    cbbEstado.SelectAll;
    Raise Exception.Create('Selecione um estado!');
  end;
  FBuscaCEP.Busca(edtLogradouro.Text, edtMunicipio.Text, EstadosBRSigla[TEstadosBR(cbbEstado.ItemIndex)]);
end;

procedure TFrmBuscaCEP.AfterConstruction;
var
  nIndex : Byte;

begin
  inherited;
  cbbEstado.Items.Clear;
  FBuscaCEP := TBuscaCEP.Create;
  dtsBuscaCEP.DataSet := FBuscaCEP.ClientDataSet;
  for nIndex := 0 to Length(EstadosBR) - 1 do
    if nIndex = 0 then
      cbbEstado.Items.Add( EstadosBR[TEstadosBR(nIndex)] )
    else
      cbbEstado.Items.Add( EstadosBR[TEstadosBR(nIndex)] + ' (' + EstadosBRSigla[TEstadosBR(nIndex)] + ')' );
  cbbEstado.ItemIndex := 0;
end;

destructor TFrmBuscaCEP.Destroy;
begin
  FreeAndNil(FBuscaCEP);
  inherited;
end;

procedure TFrmBuscaCEP.edtCEPKeyPress(Sender: TObject; var Key: Char);
begin
  if not CharInSet(Key, ['0'..'9', #8, #9, #13]) then
    Key := #0;
end;

procedure TFrmBuscaCEP.SetCEP(const Value: String);
begin
  FCEP := Value;
  edtCEP.Text := FCEP;
end;

end.
